CVSREPO=:ext:gray@puszcza.gnu.org.ua:/webcvs/xenv
SOURCES=index.html style.css

index.html: index.org
	emacs --batch -l org2html.el index.org

clean: index.html

.PHONY: .cache
.cache:
	@if test -d .cache ; then \
	  cd .cache && cvs update ; \
	else \
          mkdir .cache && \
	  cvs -z3 -d $(CVSREPO) co -d .cache xenv; \
	fi

update: .cache index.html
	rsync -az $(SOURCES) .cache
	cd .cache && cvs commit -m 'Update'
